import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';
import _ from 'lodash';

const app = express();
app.use(cors());

const getPcData = async function () {
  const url = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

  let data = {};

  try {
    let response = await fetch(url);
    data = await response.json();
  } catch (error) {
    console.log('An error occured: ', error);
  }

  return data;
};

app.get('/volumes', async (req, res) => {
  let data = await getPcData();
  let groups = _.groupBy(data.hdd, 'volume');

  let sum = _.mapValues(groups, function (volumes, key) {
    return volumes.reduce((carry, volume) => { return carry + volume.size }, 0) + 'B';
  });

  res.json(sum);
});

app.get('/:path(*)', async (req, res) => {
  let path = _.trim((req.params.path || ''), '/');
  let data = await getPcData();

  if (path === '') {
    return res.json(data);
  }

  let parts = path.split('/');

  if (parts.indexOf('length') > 0 || !_.has(data, parts) ) {
    return res.sendStatus(404);
  }

  return res.json(_.get(data, parts));
});

app.listen(3000, function () {
  console.log('Listening on port 3000!');
});
